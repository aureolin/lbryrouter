### LBRY Router
This is a simple implementation of a router that can be set up on a domain to create email-client friendly links for lbry URLs. It takes the request path as the destination LBRY URL, so `https://dir.block.ng/<lbryurl>` redirects to `lbry://<lbryurl>` for instance.

#### Examples
* *Anonymous Claim* - https://dir.block.ng/20000daysonearth redirects to lbry://20000daysonearth
* *Claim by Publisher* - https://dir.block.ng/@EconStories/fightofthecentury redirects to lbry://@EconStories/fightofthecentury
* *Publisher* - https://dir.block.ng/@oscopelabs redirects to lbry://@oscopelabs

#### Configuration
Sample web server configuration files are provided as `apache.vhost.conf` (for Apache) and `nginx.site.conf` (for nginx).
